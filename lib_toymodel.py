#!/usr/bin/env python
# lib_toymodel.py

"""
Author : peters 

Revision History:
File created on 16 Dec 2009.

This program demonstrates the different ways to optimize a system with 3 unknowns given a set of observations 
and prior information on the PDF of the unknowns. There are currently three options:

(1) a maximum likelihood estimate: this estimate is a simple linear algebra solution to an overconstrained problem. It does not use
    prior information on parameters or their uncertainty as it is assumed that a unique solution exists. It does return an estimate of
    the uncertainty in the optimal parameters. This method is an alternative to the type of minimization routines that come standard with
    many software packages such as IDL, MATLAB, python, etc.

(2) a full Kalman Filter estimate: this method is also suitable when there are more unknowns that observations, or when the solution is 
    not uniquely defined. In that case, a-priori information on the probability density (PDF) of the solution is used. The method returns an
    optimal parameter set and its full covariance.

(3) an ensemble Kalman Filter estimate: the Kalman filter equations are again solved, but now using the statistics of an ensemble to 
    find the optimum. This solution is in principle inferior to the one above (statistical errors) but sometimes easier to calculate when
    the structure of the underlying model is complex. The method returns an ensemble of solutions to the problem, from which a mean and covariance 
    can be derived that is the same as in the full Kalman filter.

The problem that we are optimizing here is simple. We have an equation of the form:

    y = a + b*x + c* x^2

and the question is whether we can find a set [a,b,c] given imperfect observations (y). These imperfect observations are created by taking a 
'perfect' y (outcome of the equation given correct [a,b,c] ) and then perturbing them with some noise. We want to then find the set [a,b,c] 
that was used, despite the noise.

In this problem, we use the following settings:

    noise       = 0.1             # white noise with 0.0 mean and 0.1 standard deviation
    N           = 20              # 20 observations, a very large number given 3 unknowns
    w           = [0.1,0.5,0.2]   # the true values of [a,b,c]
    wb          = [0.5,0.5,0.5]   # first-guess parameter values, needed in the Kalman filters
    Pb          = [2.5,2.5,2.5]   # first-guess parameter uncertainties, needed in the Kalman filters. Note that these 
                                  # are large so they don't have much influence
    nmembers    = 1000            # number of ensemble members, also large to prevent noisy solution.

"""

from pylab import *
import numpy.linalg
import ipywidgets as widgets
from ipywidgets import interact, interactive

# from IPython.html.widgets import interact, interactive
# from IPython.html import widgets
from IPython.display import display
from ipywidgets import Button, HBox, VBox
import os,sys,glob, copy
matplotlib.rcParams.update({'font.size': 12})


class Minimize(object):
    """ This class holds all methods for the practical """
    
    def __init__(self):
        
        self.randstate = np.random.get_state()
        self.observationoperator = self.simplefunction  # set up association
        self.minimizer=self.unscented_kalman_filter
        
        minmethods = ['maximum likelihood','kalmanfilter','ensemble kalman filter', 'unscented_kalman_filter']
        self.w1 = interact(self.selectminimizer,method=minmethods)
        
    def resetstate(self):
        """ reset the random state to the value when we first imported the module, this allows repeatable statistics """
        np.random.set_state(self.randstate)
        

    def selectminimizer(self,method='unscented_kalman_filter'):
        
        self.ensemblemethod=False
        if method == 'kalmanfilter':
            self.minimizer = self.kalmanfilterestimate
        if method == 'maximum likelihood':
            self.minimizer = self.maximumlikelihoodestimate
        elif method == 'ensemble kalman filter':
            self.minimizer = self.fullensemblekalmanfilterestimate
            self.ensemblemethod=True
        elif method == 'unscented_kalman_filter':
            self.minimizer = self.unscented_kalman_filter

        functions = 'Sinusodal', 'Exponential', 'Quadratic'
        try:
            self.w2.widget.close()
        except:
            pass
        self.w2 = interact(self.startvalues,\
                      functype=widgets.Dropdown(value='Exponential', description='Type of function', options=functions),
                      Nobs=widgets.IntText(value=50,description='# obs'),\
                      Scov=widgets.FloatText(value=2.0,description='stddev of params'),\
                      Snoise=widgets.FloatText(value=1.0,description='stddev of obs'),\
                      nmem=widgets.IntText(value=10,description='# members',disabled=not(self.ensemblemethod)),\
                      rnd=widgets.Checkbox( value=False, description='Freeze Random', disabled=False )) 

    def startvalues(self,Nobs=5,Scov=2.0,Snoise=2.0,nmem=10,rnd=False, functype='Quadratic'):
       
        if rnd:
            self.resetstate() # always work with same random number set
        else:    
            self.randstate = np.random.get_state()
            
        self.N       = Nobs   # number of obs. Increasing N gives better estimate
        self.noise   = Snoise
        self.init_params  = [0.0, 0.0, 0.0]
        #self.init_params  = np.random.randn(3)
        self.true_params  = [0.1, 0.1, 0.1]
        self.params = [0.5, 0.5, 0.5]
        self.init_cov  = identity(len(self.true_params))*Scov**2
        self.nmembers= nmem
        self.functype = functype
        #
        # Set up the problem by creating the time series x and y, making a linear model H, and a set of perturbed obs y'
        #
        whitenoise=randn(self.N)*self.noise
        # 
        # true parameters w, to be retrieved from noisy data 
        # 
        #
        # The time series x for which we evaluate our model, we take 100 values to define the system
        #
        xf=-4.0+arange(100)*(8.0/100)  # 100 values between -2 and 6

        # sample the full time series randomnly N times, this gives a subset of values 

        x=[]
        for i in range(self.N):
            ii = int(100*rand())
            x.append(xf[ii])
        x=array(x)
        x.sort()
        
        self.alltimes = xf
        self.obstimes = x
        
        
        # Generate a limited set of outcomes from our model 
        #
        pseudo_obs=self.observationoperator(self.obstimes,self.true_params)
        
        # 
        # add the noise, these are our observed values y
        #
        self.obsvalues=pseudo_obs+whitenoise
        #
        #
        # Optionally, make a matrix of weights for each observations as indicated by it's noise
        # For now, assume white noise is okay
        #
        self.R=zeros((self.N,self.N),float)
        for i in range(self.N): self.R[i,i]=self.noise**2
            
        self.Hfull=transpose(array([ones(100,float),self.alltimes,self.alltimes**2]))
        #
        # Make a subset of H that only maps onto the N observations we have
        #
        self.H=transpose(array([ones(self.N,float),self.obstimes,self.obstimes**2]))
        
        self.run()
        self.statistics()
        self.visualize()


    def simplefunction(self, x, w):
        """ this performs a model run given x-values and a parameter set w=[a,b,c]"""
        if self.functype == 'Sinusodal':
            return w[0]+sin(w[1])*x+w[2]*x*x  # this is a nonlinear model
        elif self.functype == 'Exponential':
            return w[0] + (1.5**w[1])*x + w[2]*x*x  # this is a nonlinear model
        elif self.functype == 'Quadratic':
            return w[0]+w[1]*x+w[2]*x*x

    def maximumlikelihoodestimate(self,H,R,y,wb,Pb):
        """ maximum likelihood solution for a system with more obs than parameters"""

        #
        # Now, make the inverse maximum likelihood estimate using the relationship:
        #
        # w_ML= (H^T H)^(-1) H^T y
        #
        w1=dot(transpose(H),dot(R,H))  # bracket term
        w2=inv(w1)          # inverse bracket
        w3=dot(w2,transpose(H)) # times X^T
        w_opt=dot(w3,dot(R,transpose(y)))  # times y
        
        # var(w,w)=sigma^2(H^TH)^(-1)
        #
        res1=(y-dot(H,w_opt))
        res2=dot(transpose(res1),res1)
        sigmasquared=1./self.N*res2
        P_opt=sigmasquared*w2
        
        return (w_opt, P_opt)
    
    def kalmanfilterestimate(self,H,R,y,wb,Pb):
        """ Full Kalman Filter matrix solution for a system with more obs than parameters"""
        # Make the KF solution:
        #
        # x_a= x_p + K (y-Hx_p)
        #

        I = identity(len(self.init_params))

        # Create full solution
        HPHR = dot(dot(H,Pb),transpose(H))+R
        K = dot(dot(Pb,transpose(H)),inv(HPHR))
        w_opt = wb + dot(K,y-dot(H,wb))
        P_opt = dot((I-dot(K,H)),Pb)     
        return(w_opt,P_opt)
    
    def fullensemblekalmanfilterestimate(self,H,R,y,wb,Pb):
        """ ensemble Kalman Filter solution for a system with more obs than parameters"""


        # Make the EnKF solution:
        #
        # x_a= x_p + K (y-Hx_p)
        #

        I = identity(3)

        # Create ensemble members

        C = linalg.cholesky(Pb)

        X_prime = zeros((3,self.nmembers),float)
        HX_prime = zeros((self.N,self.nmembers),float)
        for n in arange(self.nmembers):
            xp = dot(transpose(C),randn(3))
            X_prime[:,n] = xp
            HX_prime[:,n] =self.observationoperator(self.obstimes,np.array(wb)+xp)-self.observationoperator(self.obstimes,wb)


        Pr = dot(X_prime,transpose(X_prime))/(self.nmembers-1) # just a check on the P matrix 

        # Create full solution, first calculate the mean of the analysis

        HPH = dot(HX_prime,transpose(HX_prime))/(self.nmembers-1) # Eq 8 in Peters 2005
        HPHR = HPH+R
        HPb = dot(X_prime,transpose(HX_prime))/(self.nmembers-1)  # Eq 9 in Peters 2005
        K = dot(HPb,inv(HPHR))                                    # Eq 4 in Peters 2005

        w_opt = wb + dot(K,y-self.observationoperator(self.obstimes,wb)) # Eq 2 in Peters 2005

        # And next make the Pa. Note that we calculate P by using the full equation (10) at once, and not in a serial update fashion as described in
        # Whitaker and Hamill. For the current problem with limited N_obs this is easier, or at least more straightforward to do.

        sHPHR = linalg.cholesky(HPHR)               # square root of HPH+R
        part1 = dot(HPb,transpose(inv(sHPHR)))      # HP(sqrt(HPH+R))^-1
        part2 = inv(sHPHR+linalg.sqrt(R))           # (sqrt(HPH+R)+sqrt(R))^-1
        Kw = dot(part1,part2)                       # K~
        X_prime_opt = dot(I,X_prime)-dot(Kw,HX_prime)
        P_opt =dot(X_prime_opt,transpose(X_prime_opt))/(self.nmembers-1)

        # Now do the adjustments of the modeled mixing ratios using the linearized ensemble

        part3 = dot(HPH,transpose(inv(sHPHR)))      # HPH(sqrt(HPH+R))^-1
        Kw = dot(part3,part2)                       # K~
        HX_opt = self.observationoperator(self.obstimes,wb)+dot(dot(HPH,inv(HPHR)),y-self.observationoperator(self.obstimes,wb))
        HX_prime_opt = HX_prime-dot(Kw,HX_prime)    
        
        return (w_opt,P_opt)
    
    def serialensemblekalmanfilterestimate(self, H, R, y, wb, Pb):
        """ ensemble Kalman Filter solution for a system with more obs than parameters, serial algorithm"""
        
        I = identity(3)

        # Create ensemble members

        C = linalg.cholesky(Pb)

        Hx=self.observationoperator(self.obstimes,wb)

        X_prime = zeros((3,self.nmembers),float)
        HX_prime = zeros((self.N,self.nmembers),float)
        for n in arange(self.nmembers): # Loop over all ensemble members
            xp = dot(C,randn(3))
            X_prime[:,n] = xp
            HX_prime[:,n] = self.observationoperator(self.obstimes,np.array(wb)+xp)-Hx

        wzero=copy.copy(wb)
        for n in range(self.N): # Loop over all the observations

            PHt                         = 1./(self.nmembers-1)*np.dot(X_prime,HX_prime[n,:])# eq 9
            HPHR                        = 1./(self.nmembers-1)*(HX_prime[n,:]*HX_prime[n,:]).sum()+R[n,n] # geen idee

            KG                          = PHt/HPHR # eq 4; '/' omdat ^(-1)

            alpha                       = np.double(1.0)/(np.double(1.0)+np.sqrt( (R[n,n])/HPHR ) ) # Eq 11. Waarom alleen het [n,n]e element van R (covariantiematrix) wordt gebruikt is onduidelijk

            res                         = y[n]-Hx[n] # difference between observations and model
            wzero                       = wzero + KG*res # Eq 2

            for r in range(self.nmembers):
                X_prime[:,r]            = X_prime[:,r]-alpha*KG*(HX_prime[n,r]) # eq 10. Update deviations from the mean state vector

                #WP !!!! Very important to first do all obervations from n=1 
                #WP through the end, and only then update 1,...,n. The current observation
                #WP should always be updated last because it features in the loop !!!!

            for m in range(n+1,self.N):
                fac                     = 1.0/(self.nmembers-1)*(HX_prime[n,:]*HX_prime[m,:]).sum()/HPHR
                Hx[m]                   = Hx[m] + fac * res # Equation 12 and 13 Peters 2005
                HX_prime[m,:]           = HX_prime[m,:] - alpha*fac*HX_prime[n,:] # Equation 12 and 13 Peters 2005
            for m in range(n+1):
                fac                     = 1.0/(self.nmembers-1)*(HX_prime[n,:]*HX_prime[m,:]).sum()/HPHR # Equation 12 and 13 Peters 2005
                Hx[m]                   = Hx[m] + fac * res # Equation 12 and 13 Peters 2005
                HX_prime[m,:]           = HX_prime[m,:] - alpha*fac*HX_prime[n,:] # Equation 12 and 13 Peters 2005

        P_opt = dot(X_prime,transpose(X_prime))/(self.nmembers-1) # Eq 7
        w_opt = wzero
        
        return(w_opt,P_opt)

    def unscented_kalman_filter(self, H, R, y, wb, Pb, M=1, alpha=1e-3, kappa=0.0, beta=2.0):
        """
        Unscented Kalman Filter - a single step
        Steps:
            1. Calculate the weights
            2a. Initialise the Sigma points
            2b. Calculate the Sigma points
            3. Propagate the Sigma points
            4. Simulate measurements
            5. Compute new statevector
        inputs:
            H (matrix):
                matrix for mapping in the forward model
                Currently unused; the observation operator is used
            R (matrix):
                Measurement error covariance matrix
            y (array):
                Measurements at time t (current time step)
            wb (array):
                parameter mean estimates from time t-1
            Pb (matrix):
                Covariance matrix for x from time t-1
            M (optional, default = 1: array/float):
                This is currently the value to propagate x from time t-1 to t within
                the dynamic model.
                How this works can be adapted in dyod.
            alpha (optional, defaul=1e-3):
                Scaling parameter to determine spread of sigma points around means
            kappa (optional, default=0):
                Secondary scaling parameter
            beta (optional, default=2):
                Scaling parameter to incorporate prior knowledge of the 
                distribution of x.
                
        outputs:
            x (array):
                parameter mean estimates at time t
            P (matrix):
                Covariance matrix for x at time t
        """
        # 0. Get the number of params:
        n = len(wb)
        
        # 1. Calculate the weights
        l = alpha**2 * (n + kappa) - n
        w0m = l / (n + l)
        w0c = l / (n + l) + (1 - alpha ** 2 + beta)
        wim = wic = 1 / (2 * (n + l))
        
        # 2. Compute sigma points
        # 2a. Initialise sigma points
        chi = np.zeros((n, 2 * n + 1))
        chibar = np.zeros_like(chi)
        
        # 2b. Calculate sigma points
        chi[:, 0] = wb
        Pb = nearestPD(Pb)
        Pb_chol = linalg.cholesky(Pb)
        for i in range(1, (n + 1)):
            chi[:, i] = wb + np.sqrt(n + l) * Pb_chol[:, (i - 1)].T
            chi[:, (n + i)] = wb - np.sqrt(n + l) * Pb_chol[:, (i - 1)].T
        
        # 3. Propagate
        chihat = dynmod(M,chi)
        self.chihat = chihat
        means = w0m * chihat[:,0] + np.sum(wim * chihat[:, 1:], axis=1)
        self.means = means
        covs = w0c*np.outer((chihat[:,0] - means),
                            (chihat[:,0]-means)) + Pb
        for i in range(1,2*n+1):
            covs += wic * np.outer((chihat[:, i] - means),
                                   (chihat[:, i] - means)) + Pb
        covs = nearestPD(covs)

        # 4. Simulate measurements
        chol = linalg.cholesky(covs)
        chibar[:,0] = means
        for i in range(1, (n + 1)):
            chibar[:,i] = means + np.sqrt(n + l) * chol[:, (i - 1)].T
            chibar[:,(n+i)] = means - (np.sqrt(n + l) * 
                                       chol[:,(i-1)].T)
        self.chibar = chibar

        Yhat = np.zeros((len(y), 2 * n + 1))
        for i in range(2 * n + 1):
            Yhat[:, i] = self.observationoperator(self.obstimes,
                                                  chibar[:, i])
        self.Yhat = Yhat
        # 5. Compute new statevector
        # mu is related to the average prediction
        mu = w0m * Yhat[:, 0] + np.sum(wim * Yhat[:, 1:], axis=1)
        self.mu = mu
        # S is the covariance in measurement space
        S = w0c * np.outer(Yhat[:,0] - mu, 
                           Yhat[:,0] - mu) + R

        #C is the Cross Co-relation Matrix between 
        # state space and predicted space
        C = w0c * np.outer(chibar[:, 0] - means, 
                           Yhat[:, 0] - mu)

        for i in range(1, 2 * n + 1):
            S += wic * np.outer(Yhat[:, i] - mu, 
                                Yhat[:, i] - mu) + R
            C += wic * np.outer(chibar[:, i] - means, 
                                Yhat[:, i] - mu)

        #Compute filtered values
        # G is the gain
        G = np.dot(C, np.linalg.inv(S))
        x = means + np.dot(G, (y - mu))
        P = covs - np.linalg.multi_dot([G, S, G.T])
        self.x = x
        self.P = P
        return x, P
        
    def run(self):
        
        outcome = self.minimizer(self.H, self.R, self.obsvalues, 
                  self.init_params, self.init_cov) 
        
        self.params = outcome[0]
        self.cov = outcome[1]
        
        return outcome
        
    def visualize(self):
        
        firstguess = self.observationoperator(self.alltimes, self.init_params)
        reconstructed = self.observationoperator(self.alltimes, self.params)
        simobs = self.observationoperator(self.obstimes, self.params)
        truemodel = self.observationoperator(self.alltimes, self.true_params)
        
        fig = plt.figure(figsize=(10, 5))
        ax1 = fig.add_axes([0.1, 0.1, 0.4, 0.8])
        ax2 = fig.add_axes([0.55, 0.1, 0.4, 0.8])
        ax1.plot(self.alltimes, truemodel, 'r-', lw=3, label='True function')
        ax1.plot(self.alltimes, reconstructed, 'g-', lw=3, label='Estimated function')
        ax1.plot(self.alltimes, firstguess, 'k.', lw=1, label='First guess function')
        #ax1.plot(self.obstimes,self.obsvalues,'bo',label='Noisy obs')
        
        ax1.errorbar(self.obstimes, self.obsvalues, 
                     yerr=self.noise, linestyle='None',
                     color='b', marker='o',lw=3,label='Noisy obs')
        ax1.grid(True)
        ax1.legend(loc=0) 
        #ax2.plot(self.alltimes,truemodel-firstguess,'ko',label='first-guess residual')
        ax2.plot(self.obstimes, self.obsvalues - simobs, 'gs',label='estimated residual')
        ax2.grid(True)
        ax2.legend(loc=0)
        
        self.ax = ax1
        
    def statistics(self):
        
        # estimate noise using the relationship:
        #
        # sigma_ML^2= 1/N (y-Xw)^T (y-Xw)
        #
       
        for i in range(len(self.params)):
            print(('Init parameter values : %4.2f±%4.2f')% \
                  (self.init_params[i], np.sqrt(self.init_cov[i, i])) )
            print(('Found parameter values: %4.2f±%4.2f')% \
                  (self.params[i], np.sqrt(self.cov[i, i])))
            print(('True parameter values : %4.2f\n')% \
                  (self.true_params[i]))
        
        res1 = (self.obsvalues - self.observationoperator(self.obstimes, self.params))
        res2 = dot(transpose(res1), res1)
        sigmasquared = 1. / self.N * res2
        #
        print('Estimated noise is     : %5.3f' % np.sqrt(sigmasquared))
        print('Prescribed noise was   : %5.3f' % self.noise )
        #
        # Estimate parameter covariance in estimated w with the relationship:
        #

        print('Model data mismatch was   : %5.3f' % (np.sqrt(self.R[0, 0]),))
        print('Achieved statistics were  : %5.3f' % (np.sqrt((res1 ** 2).mean()),))
     
        stats = []
        for i in range(len(self.params)):
            stats.append('%5.3f±%5.3f' % (self.params[i], np.sqrt(self.cov[i, i]))) 
        items = [Button(description=w) for w in stats]
        box=HBox([VBox([items[0], items[1]]), VBox([items[2], items[2]])])
        self.w2 = box
        
def dynmod(M,X):
    """
    The 'dynamic model', i.e. how x_t-1 and x_t are correlated.
    # Currently this is equivalent to a random walk type 1 in log-space.
    In CTDAS, this is just the Identity
    """    
    return(M * X)
        
def nearestPD(matrix):
    """Find the nearest positive-definite matrix to input

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    taken from https://stackoverflow.com/questions/43238173/python-convert-matrix-to-positive-semi-definite
    """

    B = (matrix + matrix.T) / 2
    _, s, V = np.linalg.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(np.linalg.norm(matrix))
    # The above is different from [1]. It appears that MATLAB's `chol` Cholesky
    # decomposition will accept matrixes with exactly 0-eigenvalue, whereas
    # Numpy's will not. So where [1] uses `eps(mineig)` (where `eps` is Matlab
    # for `np.spacing`), we use the above definition. CAVEAT: our `spacing`
    # will be much larger than [1]'s `eps(mineig)`, since `mineig` is usually on
    # the order of 1e-16, and `eps(1e-16)` is on the order of 1e-34, whereas
    # `spacing` will, for Gaussian random matrixes of small dimension, be on
    # othe order of 1e-16. In practice, both ways converge, as the unit test
    # below suggests.
    I = np.eye(matrix.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(np.linalg.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3

def isPD(matrix):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = np.linalg.cholesky(matrix)
        return True
    except np.linalg.LinAlgError:
        return False

if __name__ == "__main__":
    test=Minimize()
