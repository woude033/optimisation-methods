# Optimisation methods 
This project includes a python library including some optimisation methods
for people that want to become more familiar with these methods.
First created by Wouter Peters, edited by Auke van der Woude


Current methods include:
1. Unscented Kalman Filter
1. Ensemble Kalman Filter
1. Maximum likelihood estimation

A notebook is included that allows easy testing of the methods.

Feel free to add and change code to see how the optimisation methods behave!
